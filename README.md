# Fract'ol

Ce programme permet d'explorer differentes fractales.

Les fractales disponibles sont :
* Mandelbrot
* Julia
* Burning ship
* Julia Burning ship
* Biomorph
* Bird of prey
* Julia Bird of prey
* Tribrot 

Le programme permet de zoomer et de se deplacer mais aussi de changer de couleurs.

Exemple:

![mandelbrot](pictures/mandelbrot.png)

Mandelbrot couleur no 1

![julia](pictures/julia.png)

Ensemble de Julia couleur no 3

![burning_ship](pictures/burning_ship.png)

Burning Ship couleur no 2

![julia_burning](pictures/julia_burning_ship.png)

Ensemble de Julia du Burning Ship 

![biomorph1](pictures/biomorph1.png)

Biomorph couleur no 2

![biomorph2](pictures/biomorph2.png)

Biomorph couleur no 2

![bird](pictures/bird.png)

Bird of Prey couleur no 1

![julia_bird](pictures/julia_bird.png)

Ensemble de Julia de Bird of Prey couleur no 1

![tribrot](pictures/tribrot.png)

Tribrot couleur no 1


Note finale : 118
