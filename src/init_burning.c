/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_burning.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:09:56 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 16:37:21 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_burning(t_env *env)
{
	env->fractal = BURNING;
	env->x1 = -1;
	env->y1 = -1.8;
	env->iter_max = 200;
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
}
