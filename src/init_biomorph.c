/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_biomorph.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:31:04 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 16:45:59 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_biomorph(t_env *env)
{
	env->fractal = BIOMORPH;
	env->x1 = -1.5;
	env->y1 = -1.5;
	env->iter_max = 200;
	set_complexe(&(env->c_julia), BIOMORPH_CR, BIOMORPH_CI);
	env->offset_x = 0;
	env->offset_y = 0;
	env->julia_static = 0;
	env->zoom = 200;
}
