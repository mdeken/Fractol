/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burning_ship.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 18:17:28 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:08:16 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

static unsigned int	color_algorithm(t_env *env, t_complexe *z, int i)
{
	if (env->color == 0)
		return (hsv_to_rgb(i / (double)env->iter_max * 360, 0.6, 1));
	else if (env->color == 1)
		return (linear_interpolation(env, i));
	else
		return (hsv_to_rgb(i / (double)env->iter_max * 360,
					fmod(z->img, 1), 1));
}

static void			iter_burning(t_env *env, int x, int y)
{
	t_complexe	c;
	t_complexe	z;
	int			i;
	double		n;
	t_complexe	tmp;

	c.reel = (x + env->offset_x) / (double)env->zoom + env->x1;
	c.img = (y + env->offset_y) / (double)env->zoom + env->y1;
	set_complexe(&z, 0, 0);
	n = z.reel * z.reel + z.img * z.img;
	i = 0;
	set_complexe(&tmp, 0, 0);
	while (i < env->iter_max && n < 4)
	{
		z.img = 2 * fabs(z.img * z.reel) + c.img;
		z.reel = tmp.reel - tmp.img - c.reel;
		tmp.reel = z.reel * z.reel;
		tmp.img = z.img * z.img;
		n = tmp.reel + tmp.img;
		i++;
	}
	if (i == env->iter_max)
		ft_pixel_to_img(env, x, y, 0x000000);
	else
		ft_pixel_to_img(env, x, y, color_algorithm(env, &z, i));
}

void				burning_ship(t_env *env)
{
	int	x;
	int	y;

	x = 0;
	while (x < WIDTH)
	{
		y = 0;
		while (y < HEIGHT)
		{
			iter_burning(env, x, y);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
