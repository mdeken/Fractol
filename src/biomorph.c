/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   biomorph.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 19:12:16 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:19:08 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

static unsigned int	color_algorithm(t_env *env, double n, t_complexe *z, int i)
{
	(void)i;
	if (fabs(z->reel) < 10)
	{
		if (env->color == 1)
			return (hsv_to_rgb(200, fabs(z->reel) / 10, 1));
		else
			return (0x000000);
	}
	else if (fabs(z->img) < 10)
	{
		if (env->color == 1)
			return (hsv_to_rgb(340, fabs(z->img) / 10, 1));
		else
			return (0xFFFFFFF);
	}
	else
	{
		if (env->color == 1)
			return (0xFFFFFF);
		else if (env->color == 0)
			return (hsv_to_rgb(n / (double)100.0, fmod(z->img, 0.9), 1));
		else
			return (hsv_to_rgb(n / (double)100, 0.6, 1));
	}
}

static void			iter_biomorph(t_env *env, int x, int y)
{
	t_complexe	z;
	int			i;
	double		n;
	double		tmp;

	z.reel = (x + env->offset_x) / (double)env->zoom + env->x1;
	z.img = (y + env->offset_y) / (double)env->zoom + env->y1;
	i = 0;
	n = z.reel * z.reel + z.img * z.img;
	while (i < env->iter_max && n < 100)
	{
		tmp = z.reel;
		z.reel = z.reel * (z.reel * z.reel - 3 * z.img * z.img)
			+ env->c_julia.reel;
		z.img = z.img * (3 * tmp * tmp - z.img * z.img) + env->c_julia.img;
		n = z.reel * z.reel + z.img * z.img;
		i++;
	}
	ft_pixel_to_img(env, x, y, color_algorithm(env, n, &z, i));
}

void				biomorph(t_env *env)
{
	int	x;
	int	y;

	x = 0;
	while (x < WIDTH)
	{
		y = 0;
		while (y < HEIGHT)
		{
			iter_biomorph(env, x, y);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
