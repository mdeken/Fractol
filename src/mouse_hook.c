/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 17:13:05 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 17:15:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	mouse_hook(int button, int x, int y, t_env *env)
{
	(void)x;
	(void)y;
	if (button == 1)
		env->julia_static = (env->julia_static == 0) ? 1 : 0;
	else if (button == 4)
		apply_zoom(env, ZOOM_IN);
	else if (button == 5)
		apply_zoom(env, ZOOM_OUT);
	draw_fractal(env);
	return (1);
}
