/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_menu.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 17:38:30 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:52:34 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	print_background(t_env *env)
{
	int	x;
	int	y;
	int	max_x;
	int	max_y;

	x = 0;
	max_x = 420;
	max_y = 200;
	if (env->menu == 0)
	{
		max_x = 250;
		max_y = 50;
	}
	while (x < max_x)
	{
		y = 0;
		while (y < max_y)
		{
			mlx_pixel_put(env->mlx, env->win, x, y, 0);
			y++;
		}
		x++;
	}
}

void		print_menu(t_env *env)
{
	print_background(env);
	if (env->menu == 0)
	{
		mlx_string_put(env->mlx, env->win, 15, 15, 0xFFFFFF,
				"Press 'h' to show help");
	}
	else
	{
		mlx_string_put(env->mlx, env->win, 15, 15, 0xFFFFFF,
				"Press 'h' to close help");
		mlx_string_put(env->mlx, env->win, 15, 40, 0xFFFFFF,
				"w/a/s/d : move");
		mlx_string_put(env->mlx, env->win, 15, 65, 0xFFFFFF,
				"+/-/mouse wheel : zoom");
		mlx_string_put(env->mlx, env->win, 15, 90, 0xFFFFFF,
				"1/2/3/4/5/6/7/8 : change fractal");
		mlx_string_put(env->mlx, env->win, 15, 115, 0xFFFFFF,
				"1/2/3 (num) : change color");
		mlx_string_put(env->mlx, env->win, 15, 140, 0xFFFFFF,
				"Arrow up/down : raise/lower iterations");
		mlx_string_put(env->mlx, env->win, 15, 165, 0xFFFFFF,
				"Esc : exit Fract'ol");
	}
}
