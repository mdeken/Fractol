/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mandelbrot.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:06:05 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 16:06:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_mandelbrot(t_env *env)
{
	env->fractal = MANDELBROT;
	env->x1 = MANDEL_XMIN;
	env->y1 = MANDEL_YMIN;
	env->iter_max = 80;
	set_complexe(&(env->c_point), 0, 0);
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
}
