/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 15:21:23 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/08 19:35:23 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "event_macos.h"

int	main(int argc, char **argv)
{
	t_env	env;

	if (argc == 1)
	{
		print_usage();
		return (-1);
	}
	init_fractal(&env, argv[1]);
	init_env(&env);
	mlx_mouse_hook(env.win, mouse_hook, &env);
	mlx_hook(env.win, KEYPRESS, SYNCKEYBOARD, key_hook, &env);
	mlx_hook(env.win, MOTIONOTIFY, POINTERMOTIONMASK, motion_hook, &env);
	mlx_loop(env.mlx);
	return (0);
}
