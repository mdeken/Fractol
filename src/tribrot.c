/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tribrot.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 15:43:08 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:25:32 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

static unsigned int	color_algorithm(t_env *env, t_complexe *z, int i)
{
	if (env->color == 0)
		return (hsv_to_rgb(i / (double)env->iter_max * 360, 0.6, 1));
	else if (env->color == 1)
		return (linear_interpolation(env, i));
	else
		return (hsv_to_rgb(i / (double)env->iter_max * 360,
					fmod(z->img, 1), 1));
}

static void			iter_tribrot(t_env *env, int x, int y)
{
	t_complexe	z;
	t_complexe	c;
	double		n;
	double		tmp;
	int			i;

	c.reel = (x + env->offset_x) / (double)env->zoom + env->x1;
	c.img = (y + env->offset_y) / (double)env->zoom + env->y1;
	set_complexe(&z, 0, 0);
	n = z.reel * z.reel + z.img * z.img;
	i = 0;
	while (i < env->iter_max && n < 4)
	{
		tmp = z.reel;
		z.reel = z.reel * (z.reel * z.reel - 3 * z.img * z.img) + c.reel;
		z.img = z.img * (3 * tmp * tmp - z.img * z.img) + c.img;
		n = z.reel * z.reel + z.img * z.img;
		i++;
	}
	if (i == env->iter_max)
		ft_pixel_to_img(env, x, y, 0x000000);
	else
		ft_pixel_to_img(env, x, y, color_algorithm(env, &z, i));
}

void				tribrot(t_env *env)
{
	int	x;
	int	y;

	x = 0;
	while (x < WIDTH)
	{
		y = 0;
		while (y < HEIGHT)
		{
			iter_tribrot(env, x, y);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
