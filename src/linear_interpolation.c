/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linear_interpolation.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 18:16:13 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 18:16:47 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

unsigned int	linear_interpolation(t_env *env, int i)
{
	int		rgb_result[3];
	double	t;

	t = i / (double)env->iter_max;
	rgb_result[0] = (1 - t) * R_C2 + t * R_C1;
	rgb_result[1] = (1 - t) * G_C2 + t * G_C1;
	rgb_result[2] = (1 - t) * B_C2 + t * B_C1;
	return (create_color(rgb_result[0], rgb_result[1], rgb_result[2]));
}
