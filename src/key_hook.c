/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 17:15:07 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 15:50:20 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "keycode_macos.h"
#include <stdlib.h>

static void	move(int keycode, t_env *env)
{
	if (keycode == KEYCODE_W)
		env->offset_y += 10;
	else if (keycode == KEYCODE_S)
		env->offset_y -= 10;
	else if (keycode == KEYCODE_A)
		env->offset_x += 10;
	else if (keycode == KEYCODE_D)
		env->offset_x -= 10;
}

static void	zoom(int keycode, t_env *env)
{
	if (keycode == KEYCODE_PLUS)
		apply_zoom(env, ZOOM_IN);
	else if (keycode == KEYCODE_MINUS)
		apply_zoom(env, ZOOM_OUT);
	if (keycode == KEYCODE_ARROW_UP)
		env->iter_max += 20;
	else if (keycode == KEYCODE_ARROW_DWN && env->iter_max > 20)
		env->iter_max -= 20;
}

static void	change_fractal(int keycode, t_env *env)
{
	if (keycode == KEYCODE_1)
		init_fractal(env, "Mandelbrot");
	else if (keycode == KEYCODE_2)
		init_fractal(env, "Julia");
	else if (keycode == KEYCODE_3)
		init_fractal(env, "Burning ship");
	else if (keycode == KEYCODE_4)
		init_fractal(env, "Julia Burning ship");
	else if (keycode == KEYCODE_5)
		init_fractal(env, "Biomorph");
	else if (keycode == KEYCODE_6)
		init_fractal(env, "Bird of prey");
	else if (keycode == KEYCODE_7)
		init_fractal(env, "Julia Bird of prey");
	else if (keycode == KEYCODE_8)
		init_fractal(env, "Tribrot");
}

static void	change_color(int keycode, t_env *env)
{
	if (keycode == KEYCODE_1_NUM)
		env->color = 0;
	else if (keycode == KEYCODE_2_NUM)
		env->color = 1;
	else if (keycode == KEYCODE_3_NUM)
		env->color = 2;
}

int			key_hook(int keycode, t_env *env)
{
	if (keycode == KEYCODE_ESC)
	{
		mlx_destroy_image(env->mlx, env->img);
		exit(0);
	}
	if (keycode == KEYCODE_H)
		env->menu = (env->menu + 1) % 2;
	else
	{
		move(keycode, env);
		zoom(keycode, env);
		change_fractal(keycode, env);
		change_color(keycode, env);
	}
	mlx_destroy_image(env->mlx, env->img);
	init_img(env);
	draw_fractal(env);
	return (keycode);
}
