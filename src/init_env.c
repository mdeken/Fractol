/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:47:10 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/14 17:39:43 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "libft.h"
#include <stdlib.h>

void	init_env(t_env *env)
{
	env->mlx = mlx_init();
	if (env->mlx == NULL)
	{
		ft_putendl_fd("Fatal error : mlx cannot initialize", 2);
		exit(-1);
	}
	env->zoom = ZOOM;
	env->win = mlx_new_window(env->mlx, WIDTH, HEIGHT, "Fract'ol");
	if (env->win == NULL)
	{
		ft_putendl_fd("Fatal error : mlx cannot create window", 2);
		exit(-1);
	}
	init_img(env);
	env->color = 0;
	env->menu = 0;
	draw_fractal(env);
}
