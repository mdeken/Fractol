/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_to_img.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 18:32:42 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/08 18:33:30 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_pixel_to_img(t_env *env, int x, int y, int color)
{
	unsigned int	bytes;
	unsigned int	d;
	unsigned int	j;

	bytes = env->bpp / 8;
	d = x * bytes + y * env->sizeline;
	j = -1;
	while (++j < bytes)
	{
		env->data[d + j] = color;
		color >>= 8;
	}
}
