/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hsv_to_rgb.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:53:36 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/08 18:40:22 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

unsigned int	hsv_to_rgb(double h, double s, double v)
{
	double	f;
	double	l;
	double	m;
	double	n;

	f = floor(h / 60);
	f = (h / 60) - f;
	l = (v * (1 - s)) * 255;
	m = (v * (1 - f * s)) * 255;
	n = (v * (1 - (1 - f) * s)) * 255;
	v *= 255;
	if (floor(h / 60) == 0)
		return (create_color((int)v, (int)n, (int)l));
	else if (floor(h / 60) == 1)
		return (create_color((int)m, (int)v, (int)l));
	else if (floor(h / 60) == 2)
		return (create_color((int)l, (int)v, (int)n));
	else if (floor(h / 60) == 3)
		return (create_color((int)l, (int)m, (int)v));
	else if (floor(h / 60) == 4)
		return (create_color((int)n, (int)l, (int)v));
	else if (floor(h / 60) == 5)
		return (create_color((int)v, (int)l, (int)m));
	else
		return (create_color(0, 0, 0));
}
