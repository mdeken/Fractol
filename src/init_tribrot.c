/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tribrot.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 15:51:49 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:59:56 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_tribrot(t_env *env)
{
	env->fractal = TRIBROT;
	env->x1 = -1.5;
	env->y1 = -1.5;
	env->iter_max = 80;
	set_complexe(&(env->c_point), 0, 0);
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
}
