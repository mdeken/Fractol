/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_usage.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:00:51 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:27:12 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	print_usage(void)
{
	ft_putendl_fd("Usage: ./fractol <fractal>\n", 2);
	ft_putendl_fd("Fractal available :", 2);
	ft_putendl_fd("\tMandelbrot", 2);
	ft_putendl_fd("\tJulia", 2);
	ft_putendl_fd("\tBurning ship", 2);
	ft_putendl_fd("\tJulia Burning ship", 2);
	ft_putendl_fd("\tBiomorph", 2);
	ft_putendl_fd("\tBird of prey", 2);
	ft_putendl_fd("\tJulia Bird of prey", 2);
	ft_putendl_fd("\tTribrot", 2);
	exit(-1);
}
