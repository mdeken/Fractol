/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_fractal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:49:37 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:02:44 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	draw_fractal(t_env *env)
{
	if (env->fractal == MANDELBROT)
		mandelbrot(env);
	else if (env->fractal == JULIA)
		julia(env);
	else if (env->fractal == BURNING)
		burning_ship(env);
	else if (env->fractal == JULIA_BURNING)
		julia_burning_ship(env);
	else if (env->fractal == BIOMORPH)
		biomorph(env);
	else if (env->fractal == BIRD)
		bird(env);
	else if (env->fractal == TRIBROT)
		tribrot(env);
	else if (env->fractal == JULIA_BIRD)
		julia_bird(env);
	else
		print_usage();
	print_menu(env);
}
