/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_julia_burning.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:12:04 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 16:37:34 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_julia_burning(t_env *env)
{
	env->fractal = JULIA_BURNING;
	env->x1 = -1.5;
	env->y1 = -1.5;
	env->iter_max = 50;
	set_complexe(&(env->c_julia), JULIA_BURNING_CR, JULIA_BURNING_CI);
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
	env->julia_static = 0;
}
