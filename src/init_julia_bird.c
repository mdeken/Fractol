/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_julia_bird.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 16:33:52 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 17:09:40 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_julia_bird(t_env *env)
{
	env->fractal = JULIA_BIRD;
	env->x1 = -1.5;
	env->y1 = -1.5;
	env->iter_max = 200;
	set_complexe(&(env->c_julia), 0.38, -0.185);
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
	env->julia_static = 0;
}
