/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_julia.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 16:07:17 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/05 16:33:47 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_julia(t_env *env)
{
	env->fractal = JULIA;
	env->x1 = -1.5;
	env->y1 = -1.2;
	env->iter_max = 200;
	set_complexe(&(env->c_julia), JULIA_CR, JULIA_CI);
	env->offset_x = 0;
	env->offset_y = 0;
	env->zoom = 200;
	env->julia_static = 0;
}
