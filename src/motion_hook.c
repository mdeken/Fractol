/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   motion_hook.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 17:36:15 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 15:36:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	motion_hook(int x, int y, t_env *env)
{
	if (x >= 0 && x <= WIDTH && y >= 0 && y <= HEIGHT)
	{
		if ((env->fractal == JULIA && env->julia_static == 0) ||
				(env->fractal == JULIA_BURNING && env->julia_static == 0) ||
				(env->fractal == BIOMORPH && env->julia_static == 0) ||
				(env->fractal == JULIA_BIRD && env->julia_static == 0))
		{
			env->c_julia.reel = (x + env->offset_x) /
				(double)env->zoom + env->x1;
			env->c_julia.img = (y + env->offset_y) /
				(double)env->zoom + env->y1;
			draw_fractal(env);
		}
		env->c_point.reel = x;
		env->c_point.img = y;
	}
	return (1);
}
