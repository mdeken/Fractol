/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fractal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 15:53:22 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 15:58:05 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "libft.h"

void	init_fractal(t_env *env, char *name)
{
	if (ft_strcmp(name, "Mandelbrot") == 0)
		init_mandelbrot(env);
	else if (ft_strcmp(name, "Julia") == 0)
		init_julia(env);
	else if (ft_strcmp(name, "Burning ship") == 0)
		init_burning(env);
	else if (ft_strcmp(name, "Julia Burning ship") == 0)
		init_julia_burning(env);
	else if (ft_strcmp(name, "Biomorph") == 0)
		init_biomorph(env);
	else if (ft_strcmp(name, "Bird of prey") == 0)
		init_bird(env);
	else if (ft_strcmp(name, "Tribrot") == 0)
		init_tribrot(env);
	else if (ft_strcmp(name, "Julia Bird of prey") == 0)
		init_julia_bird(env);
	else
		print_usage();
}
