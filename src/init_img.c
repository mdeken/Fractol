/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_img.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 18:46:07 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/08 18:53:15 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "libft.h"
#include <stdlib.h>

void	init_img(t_env *env)
{
	char	*data;
	int		bpp;
	int		sizeline;
	int		endian;

	env->img = mlx_new_image(env->mlx, WIDTH, HEIGHT);
	if (env->img == NULL)
	{
		ft_putendl_fd("Fatal error : mlx cannot create new image", 2);
		exit(-1);
	}
	data = mlx_get_data_addr(env->img, &bpp, &sizeline, &endian);
	env->data = data;
	env->bpp = bpp;
	env->sizeline = sizeline;
	env->endian = endian;
}
