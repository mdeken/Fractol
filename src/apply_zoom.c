/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   apply_zoom.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 19:00:23 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 16:45:41 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	apply_zoom(t_env *env, int zoom)
{
	if (zoom == ZOOM_IN)
	{
		env->zoom *= 1.5;
		env->offset_x = (env->offset_x + ((WIDTH - (WIDTH / 1.5)) / 2) +
				(env->c_point.reel - WIDTH / 2) * 0.333) * 1.5;
		env->offset_y = (env->offset_y + ((HEIGHT - (HEIGHT / 1.5)) / 2) +
				(env->c_point.img - HEIGHT / 2) * 0.333) * 1.5;
		if (env->iter_max < 800)
			env->iter_max += 10;
	}
	else if (zoom == ZOOM_OUT)
	{
		if (env->zoom > 50)
		{
			env->zoom *= 0.7;
			env->offset_x = (env->offset_x + ((WIDTH - (WIDTH / 0.7)) / 2) +
				(env->c_point.reel - WIDTH / 2) * (1 - (1 / 0.7))) * 0.7;
			env->offset_y = (env->offset_y + ((HEIGHT - (HEIGHT / 0.7)) / 2) +
				(env->c_point.img - HEIGHT / 2) * (1 - (1 / 0.7))) * 0.7;
			if (env->iter_max > 50)
				env->iter_max -= 10;
		}
	}
}
