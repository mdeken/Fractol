/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keycode_macos.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 17:16:23 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/15 14:52:13 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYCODE_MACOS_H
# define KEYCODE_MACOS_H

# define KEYCODE_ESC		53
# define KEYCODE_F1			122
# define KEYCODE_F2			120
# define KEYCODE_F3			99
# define KEYCODE_F4			118
# define KEYCODE_F5			96
# define KEYCODE_F6			97
# define KEYCODE_F7			98
# define KEYCODE_F8			100
# define KEYCODE_F9			101
# define KEYCODE_F10		109
# define KEYCODE_F11		103
# define KEYCODE_F12		111
# define KEYCODE_F13		105
# define KEYCODE_F14		107
# define KEYCODE_F15		113
# define KEYCODE_F16		106
# define KEYCODE_F17		64
# define KEYCODE_F18		79
# define KEYCODE_F19		80
# define KEYCODE_TILDE		50
# define KEYCODE_1			18
# define KEYCODE_2			19
# define KEYCODE_3			20
# define KEYCODE_4			21
# define KEYCODE_5			23
# define KEYCODE_6			22
# define KEYCODE_7			26
# define KEYCODE_8			28
# define KEYCODE_9			25
# define KEYCODE_0			29
# define KEYCODE_DASH		27
# define KEYCODE_EQUAL		25
# define KEYCODE_BACKSPC	51
# define KEYCODE_TAB		48
# define KEYCODE_Q			12
# define KEYCODE_W			13
# define KEYCODE_E			14
# define KEYCODE_R			15
# define KEYCODE_T			17
# define KEYCODE_Y			16
# define KEYCODE_U			32
# define KEYCODE_I			34
# define KEYCODE_O			31
# define KEYCODE_P			35
# define KEYCODE_A			0
# define KEYCODE_S			1
# define KEYCODE_D			2
# define KEYCODE_F			3
# define KEYCODE_G			5
# define KEYCODE_H			4
# define KEYCODE_J			38
# define KEYCODE_K			40
# define KEYCODE_L			37
# define KEYCODE_Z			6
# define KEYCODE_X			7
# define KEYCODE_C			8
# define KEYCODE_V			9
# define KEYCODE_B			11
# define KEYCODE_N			45
# define KEYCODE_M			46
# define KEYCODE_BRCKT_L	33
# define KEYCODE_BRCKT_R	30
# define KEYCODE_BACKSLSH	42
# define KEYCODE_SEMICOLON	41
# define KEYCODE_QUOTE		39
# define KEYCODE_RETURN		36
# define KEYCODE_COMMA		43
# define KEYCODE_POINT		47
# define KEYCODE_SLASH		44
# define KEYCODE_SHIFT_R	258
# define KEYCODE_CAPSLCK	272
# define KEYCODE_SHIFT_L	257
# define KEYCODE_CTRL_L		256
# define KEYCODE_ALT_L		261
# define KEYCODE_CMD_L		259
# define KEYCODE_SPC		49
# define KEYCODE_CMD_R		260
# define KEYCODE_ALT_R		262
# define KEYCODE_CTRL_R		269
# define KEYCODE_FN			279
# define KEYCODE_HOME		115
# define KEYCODE_PAGEUP		116
# define KEYCODE_DEL		117
# define KEYCODE_END		119
# define KEYCODE_PAGEDWN	121
# define KEYCODE_ARROW_UP	126
# define KEYCODE_ARROW_L	123
# define KEYCODE_ARROW_DWN	125
# define KEYCODE_ARROW_R	124
# define KEYCODE_CLR		71
# define KEYCODE_EQUAL_NUM	87
# define KEYCODE_SLASH_NUM	75
# define KEYCODE_STAR		67
# define KEYCODE_1_NUM		83
# define KEYCODE_2_NUM		84
# define KEYCODE_3_NUM		85
# define KEYCODE_4_NUM		86
# define KEYCODE_5_NUM		87
# define KEYCODE_6_NUM		88
# define KEYCODE_7_NUM		89
# define KEYCODE_8_NUM		91
# define KEYCODE_9_NUM		92
# define KEYCODE_0_NUM		82
# define KEYCODE_POINT_NUM	65
# define KEYCODE_MINUS		78
# define KEYCODE_PLUS		69
# define KEYCODE_ENTER_NUM	76

#endif
