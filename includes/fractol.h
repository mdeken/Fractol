/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 15:41:55 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/18 17:10:59 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <mlx.h>

# define WIDTH				600
# define HEIGHT				600
# define ZOOM				200
# define ZOOM_IN			1
# define ZOOM_OUT			-1
# define JULIA_CR			-0.775
# define JULIA_CI			0.177
# define JULIA_BURNING_CR	0.335
# define JULIA_BURNING_CI	0.135
# define BIOMORPH_CR		1.345
# define BIOMORPH_CI		-0.1
# define MANDEL_XMIN		-2
# define MANDEL_YMIN		-1.3
# define R_C1				0
# define G_C1				0
# define B_C1				0xFF
# define R_C2				0xFF
# define G_C2				0x80
# define B_C2				0

enum	e_fractal_type{MANDELBROT, JULIA, BURNING, JULIA_BURNING,
	BIOMORPH, BIRD, JULIA_BIRD, TRIBROT};

typedef struct			s_complexe
{
	double				reel;
	double				img;
}						t_complexe;

typedef struct			s_env
{
	void				*mlx;
	void				*win;
	void				*img;
	char				*data;
	int					bpp;
	int					sizeline;
	int					endian;
	enum e_fractal_type	fractal;
	int					iter_max;
	double				x1;
	double				y1;
	double				zoom;
	double				offset_x;
	double				offset_y;
	t_complexe			c_point;
	int					julia_static;
	t_complexe			c_julia;
	int					color;
	int					menu;
}						t_env;

void					apply_zoom(t_env *env, int zoom);
void					biomorph(t_env *env);
void					bird(t_env *env);
void					burning_ship(t_env *env);
unsigned int			create_color(int r, int g, int b);
void					draw_fractal(t_env *env);
void					ft_pixel_to_img(t_env *env, int x, int y, int color);
unsigned int			hsv_to_rgb(double h, double s, double v);
void					init_biomorph(t_env *env);
void					init_burning(t_env *env);
void					init_bird(t_env *env);
void					init_env(t_env *env);
void					init_fractal(t_env *env, char *name);
void					init_img(t_env *env);
void					init_julia(t_env *env);
void					init_julia_bird(t_env *env);
void					init_julia_burning(t_env *env);
void					init_mandelbrot(t_env *env);
void					init_tribrot(t_env *env);
void					julia(t_env *env);
void					julia_bird(t_env *env);
void					julia_burning_ship(t_env *env);
int						key_hook(int keycode, t_env *env);
unsigned int			linear_interpolation(t_env *env, int i);
void					mandelbrot(t_env *env);
int						motion_hook(int x, int y, t_env *env);
int						mouse_hook(int button, int x, int y, t_env *env);
void					print_usage();
void					set_complexe(t_complexe *z, double r, double i);
void					print_menu(t_env *env);
void					tribrot(t_env *env);

#endif
