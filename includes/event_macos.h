/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_macos.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 18:57:44 by mdeken            #+#    #+#             */
/*   Updated: 2016/03/08 18:59:35 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVENT_MACOS_H
# define EVENT_MACOS_H

# define KEYPRESS			2
# define SYNCKEYBOARD		4
# define MOTIONOTIFY		6
# define POINTERMOTIONMASK	(1L<<6)

#endif
